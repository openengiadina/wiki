SOURCES := $(shell ls -1 *.org)
HTML := $(patsubst %.org,%.html,$(SOURCES))

TMPL := template.html
OUT := public/
ASSETS := assets/

all: $(HTML) assets
html: $(HTML)

%.html: %.org
	mkdir -p $(dir $(OUT)/$(dir $@))
	pandoc \
		--from org+tex_math_single_backslash \
		--to html5+smart \
		--template=$(TMPL) \
		--css "style.css" \
		--title-prefix "openEngiadina wiki" \
		$< | sed 's/href="\([^":]*\)\.org"/href="\1.html"/g' > $(OUT)/$@

.PHONY: assets
assets:
	mkdir -p $(OUT)
	cp -r $(ASSETS)/* $(OUT)

clean:
	rm -rf $(OUT)

.PHONY: clean
