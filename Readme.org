#+TITLE: Readme

The openEngiadina wiki is a collaborative collection of notes and links on things related to the openEngiadina project.

* Contributing

Articles are org-mode files (~*.org~) in the [[https://gitlab.com/openengiadina/wiki][wiki repository]].

To add new content, fork the repository, add some content, commit and push (possibly create a merge request if you do not have push access yet).

There is no hierarchy of notes. As an entry point use the [[file:index.org][index]]. See also [[https://en.wikipedia.org/wiki/Zettelkasten][Zettelkasten]] for inspiration on this type of wiki/Knowledge base.

** org-roam

For best experience use [[https://www.orgroam.com/manual.html][org-roam]].

You might want to set your following in your ~.dir-locals.el~:

#+BEGIN_SRC
((nil . ((org-roam-directory . "/path/to/this/repo/"))))
#+END_SRC

* Exporting

The entire wiki can be exported as HTML files:

#+BEGIN_SRC
make
#+END_SRC

This exports all articles as interlinked HTML to the ~public/~ directory.

* TODOs

Things that need to be done to improve the wiki.

** TODO Multilingual

Some work is required to make the wiki work with multilingual content.

** TODO Export backlinks

Currently only forward links are exported. It would be nice to have backlinks as well (see articles that refer current article).
** TODO Internal/External links

Indicate with an icon if link is external or remains in Wiki.
