#+TITLE: CommonsPub

A project to create a general ActivityPub server. Implemented in Elixir.

Project has a very cool logo and very similar goals as [[file:cpub.org][CPub]]. Implementation is
very different. CommonsPub stores ActivityPub objects as JSON blobs (like
[[file:pleroma.org][Pleroma]]) and exposes data via a GraphQL endpoint. They don't do any [[file:semantic_web.org][Semantic Web]] stuff.

As of early 2020 development of CommonsPub has resumed as a fork of [[file:moodlenet.org][MoodleNet]]
(which itself was a fork of the original CommonsPub code, which was at least
partially a fork of [[file:pleroma.org][Pleroma]]).

* See also
** [[https://commonspub.org][CommonsPub]]

Project website.

** [[file:valueflows.org][Valueflows]]

Apparently there is an ongoing effort to use CommonsPub for the [[file:valueflows.org][Valueflows]] ontology.
