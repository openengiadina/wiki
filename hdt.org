#+TITLE: HDT

An efficient binary format for RDF.

* Could be used for
** OpenStreetMap data
** Dumps of client data
** Sneakernet distribution of data
* See also
** [[http://www.rdfhdt.org/][HDT]]

HDT website.

** [[http://kasei.us/archives/2018/11/29/hdt][Thoughts on HDT -  Gregory Todd Williams]]

A blog post describing the experience of implementing HDT in Swift ([[https://github.com/kasei/swift-hdt][swift-hdt]]).
