#+TITLE: InfoCentral
#+ROAM_KEY: https://infocentral.org/

#+BEGIN_QUOTE
An Architectural Approach to Decentralization
#+END_QUOTE

A Semantic Web inspired data model for decentralized systems.

Addresses issues with RDF in decentralized settings, notably data is stored in a
[[file:content_addressed_storage.org][content-addressable storage]].
