#+TITLE: Linked Data Fragments

Problem: SPARQL endpoints are expensive and unreliable.

Idea: Provide "simpler" or "less smart" endpoints at servers and do more
computation on client-side. This reduces load on servers, increases availability
and allows things like federated queries.

* TODO Read up on how this works
* See also
** [[https://linkeddatafragments.org/publications/jws2016.pdf][Triple Pattern Fragments: a Low-cost Knowledge Graph Interface for the Web (2016)]]

Main paper and general reference.

** [[https://linkeddatafragments.org/publications/iswc2014.pdf][Querying Datasets on the Web with High Availability (2014)]]

Paper motivating development.
