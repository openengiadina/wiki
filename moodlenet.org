#+TITLE: MoodleNet

MoodleNet is a federated platform for sharing educational resources (from Moodle).

* See also
** [[https://moodle.net/][MoodleNet]]

Project website.
** [[https://blog.moodle.net/][MoodleNet Blog]]
