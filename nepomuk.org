#+TITLE: NEPOMUK - The Social Semantic Desktop
#+ROAM_ALIAS: NEPOMUK
#+ROAM_KEY: https://nepomuk.semanticdesktop.org/

A project for extending the personal desktop to a collaboration environment based on Semantic Web ideas.

The project has published a number of ontologies that might be useful for [[file:geopub.org][GeoPub]]: [[https://www.semanticdesktop.org/ontologies/][OSCAF Ontologies]].
