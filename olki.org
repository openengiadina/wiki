#+TITLE: OLKi

[[https://olki.loria.fr/platform/][OLKi]] is a platform for federated scientific publication and discussion using [[file:activitypub.org][ActivityPub]].

* Related projects
** [[file:nanopublications.org][Nanopublications]]
