#+TITLE: Ontologies
#+ROAM_ALIAS: ontology

Ontologies describe groupings of concepts that can be reused.

Some interesting principles on how to design ontologies: [[file:towards_principles_for_the_design_of_ontologies_used_for_knowledge_sharing_1993.org][Towards Principles for the Design of Ontologies Used for Knowledge Sharing (1993)]]

* Tools for creating ontologies

[[https://protege.stanford.edu][Protege]] seems to be the go-to tool for developing ontologies.

* Existing RDF ontologies

A list of interesting ontologies/vocabularies.

** [[file:dublin_core.org][Dublin Core]]

A popular vocabulary for metadata (e.g. ~dcterms:createdAt~).

** [[file:schema_org.org][schema.org]]

Describes the things that Google understands.

** [[file:lifecycle_schema.org][Lifecycle Schema]]

Abstract ontology for workflows such as tasks and issues.

** [[file:prov.org][PROV]]

Provenance - the origin of things.

** [[file:ontologies_for_emergency_information.org][Ontologies for emergency information]]*
** [[file:essglobal.org][ESSGLOBAL]]
** [[file:valueflows.org][Valueflows]]
** [[file:linked_language_resources.org][Linked Language Resources]]

Ontology for referring to languages proper (not only language tag of literal).

** [[https://www.semanticdesktop.org/ontologies/][OSCAF Ontologies]]

Developed as part of [[file:nepomuk.org][NEPOMUK]].
** [[https://privatealpha.com/ontology/ibis/1##osd][The IBIS (bis) Vocabular]]

A vocabulary for [[file:issue_based_information_system.org][Issue-based information system]].
