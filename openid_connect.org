#+title: OpenID Connect

An extenstion to [[file:oauth_2_0.org][OAuth 2.0]] that provides an identity layer.

[[file:cpub.org][CPub]] and [[file:geopub.org][GeoPub]] implement (or strive to) OpenID Connect.

* See also
** [[https://openid.net/specs/openid-connect-core-1_0.html][OpenID Connect Core 1.0]]

The core OpenID Connect specification.

** [[https://openid.net/specs/openid-connect-discovery-1_0.html][OpenID Connect Discovery 1.0]]

Specifies an extension to OpenID Connect that allows dynamic discovery of the appropriate identity provider for a user. Uses [[file:webfinger.org][WebFinger]].

** [[file:re_claimid.org][re:claimID]]

A decentralized identity framework based on [[file:gnunet.org][GNUNet]] that provides an OpenID Connect interface.
