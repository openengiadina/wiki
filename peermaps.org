#+TITLE: PeerMaps

PeerMaps makes [[file:openstreetmap.org][OpenStreetMap]] data usable in a decentralized and offline-friendly way.

They use Dat/Hypercore.

* See also
** [[https://peermaps.org/][PeerMaps]]

Project website.
