#+TITLE: schema.org

A [[file:ontologies.org][ontology]] for almost everyting, trough the eyes of [[file:big_tech.org][Big Tech]].

Widely used all over the Web. Most notably used for Search-Engine-Optimization (SEO). Google has guides on how to add schema.org markup on your website so that Google can read the data: [[https://developers.google.com/search/docs/data-types/event][Get your event on Google]].

* See also
** [[https://schema.org/][Project website]]
