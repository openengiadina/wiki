#+TITLE: Describing Linked Datasets with the VoID Vocabulary
#+ROAM_KEY: https://www.w3.org/TR/void/
#+ROAM_ALIAS: VoID

A vocabulary for describing Datasets with metadata.

* well-known URI

Specifies a well-known URI for VoID metadata.

This sounds reasonable for CPub.
