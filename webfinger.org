#+title: WebFinger

[[https://tools.ietf.org/html/rfc7033][WebFinger (RFC 7033)]] is a protocol for discovering information about peopler over the internet.

WebFinger is used on the [[file:activitypub.org][Fediverse]] (in particular by [[file:mastodon.org][Mastodon]]) to identify users with short simpler identifiers.
